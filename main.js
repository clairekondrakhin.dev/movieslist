// tableau avec la liste de films
const movieList = [
  {
    title: "Avatar",
    releaseYear: 2009,
    duration: 162,
    director: "James Cameron",
    actors: [
      "Sam Worthington",
      "Zoe Saldana",
      "Sigourney Weaver",
      "Stephen Lang",
    ],
    description:
      "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
    rating: 7.9,
  },
  {
    title: "300",
    releaseYear: 2006,
    duration: 117,
    director: "Zack Snyder",
    actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
    description:
      "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
    rating: 7.7,
  },
  {
    title: "The Avengers",
    releaseYear: 2012,
    duration: 143,
    director: "Joss Whedon",
    actors: [
      "Robert Downey Jr.",
      "Chris Evans",
      "Mark Ruffalo",
      "Chris Hemsworth",
    ],
    description:
      "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    rating: 8.1,
  },
];

// création de la boucle permettant d'afficher la liste des films, avec toutes les informations qui les composent
for (const listing of movieList) {
  console.log(listing);
}

// Ajout de mon film à la liste
movieList.push({
  title: "Kill bill",
  releaseYear: 2003,
  duration: 111,
  director: "Quentin Tarantino",
  actors: ["Uma Thurman", "Lucy Liu"],
  description:
    "It stars Uma Thurman as the Bride, a former assassin who swears revenge on a group of assassins (Lucy Liu, Michael Madsen, Daryl Hannah, and Vivica A. Fox) and their leader, Bill (David Carradine), who tried to kill her and her unborn child. Her journey takes her to Tokyo, where she battles the yakuza.",
  poster:
    "https://m.media-amazon.com/images/M/MV5BNmFiYmJmN2QtNWQwMi00MzliLThiOWMtZjQxNGRhZTQ1MjgyXkEyXkFqcGdeQXVyNzQ1ODk3MTQ@._V1_.jpg",
  rating: 8.2,
});

//je réaffiche ma liste avec l'ajout du film kill  bill
for (const listing of movieList) {
  console.log(listing);
}

// Je supprime de la liste le film que j'aime le moins
const movieToRemove = 1; // je supprime le 2ème film de mon tableau soit à la position 1
movieList.splice(movieToRemove, 1); // ici avec l'argument 1 j'indique la suppression d'un seul film

//je réaffiche ma liste avec la suppression de
for (const listing of movieList) {
  console.log(listing);
}

// Boucle pour créer et afficher les éléments HTML
for (const movie of movieList) {
  const movieElement = document.createElement("div");
  movieElement.classList.add("movie");

  // Création des éléments HTML avec les informations du film
  //Titre du film
  const titleElement = document.createElement("h2");
  titleElement.textContent = movie.title;
  titleElement.classList.add("movieTitle");

 //Image du film
  const posterElement = document.createElement("img");
  posterElement.classList.add("imgmovie")
  posterElement.src = movie.poster;
  posterElement.alt = movie.title + " Poster";

  //année de réalisation
  const releaseYearElement = document.createElement("p");
  releaseYearElement.textContent = "Année de réalisation: " + movie.releaseYear;

  //Durée du film
  const durationElement = document.createElement("p");
  durationElement.textContent = "Durée du film: " + movie.duration + " minutes";

    //Réalisateur
  const directorElement = document.createElement("p");
  directorElement.textContent = "Réalisateur: " + movie.director;

  //Description du film
  const descriptionElement = document.createElement("p");
  descriptionElement.textContent = "Synopsis: " + movie.description;

  //note du film
  const ratingElement = document.createElement("p");
  ratingElement.textContent = "Rating: " + movie.rating;

 //acteurs principaux
  const actorsElement = document.createElement("p");
  actorsElement.textContent = "Acteurs principaux: " + movie.actors;


  // Ajout des éléments au conteneur du film
  movieElement.appendChild(titleElement);
  movieElement.appendChild(posterElement);
  movieElement.appendChild(releaseYearElement);
  movieElement.appendChild(durationElement);
  movieElement.appendChild(directorElement);
  movieElement.appendChild(actorsElement);
  movieElement.appendChild(descriptionElement);
  movieElement.appendChild(ratingElement);

  // Ajout du conteneur du film à la page
  document.body.appendChild(movieElement);
}
